package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.module.WavelabsEmployee;

public interface WavelabsRepository extends JpaRepository<WavelabsEmployee, String>
{

}
