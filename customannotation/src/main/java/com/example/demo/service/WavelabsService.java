package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Isolation;

import com.example.demo.module.WavelabsEmployee;
import com.example.demo.repository.WavelabsRepository;

@Service
public class WavelabsService {
	@Autowired
	private WavelabsRepository wavelabsRepository;

	public WavelabsEmployee saveEmployee(WavelabsEmployee employee) {
		return wavelabsRepository.save(employee);
	}

	@Cacheable(cacheNames = "mycache")
	public List<WavelabsEmployee> getAllEmployee() {
		return wavelabsRepository.findAll();
	}

	@CachePut(cacheNames = "mycache", key = "#employee.id")
	@Transactional(rollbackOn = Exception.class)
	public WavelabsEmployee updateEmployee(WavelabsEmployee employee, String id) {
		employee.setId(id);
		return wavelabsRepository.save(employee);
	}

}
